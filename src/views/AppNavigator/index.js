import React, { Component } from 'react';
import Categories from '../Categories';
import {
  Platform,
  StyleSheet,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

const AppNavigator = StackNavigator({
  ListScreen: {
    screen: () => (<Categories />) ,
    navigationOptions: ({navigation}) => ({
      title: 'Categories',
    }) 
  },
},
{
  headerMode: 'float',
});

export default AppNavigator;
