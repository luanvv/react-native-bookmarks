import {applyMiddleware, combineReducers, createStore} from 'redux';
import fetchMiddleware from './middleware/fetchMiddleware';
import bookmarks from './modules/bookmarks/reducer';
import categories from './modules/categories/reducer';
import network from './modules/network/reducer';
import {persistStore, autoRehydrate} from 'redux-persist';
import {AsyncStorage} from 'react-native';
const reducers = combineReducers({
    bookmarks,
    categories,
    network,
});

const createAppStore = applyMiddleware(fetchMiddleware)(createStore);
const store = autoRehydrate()(createAppStore)(reducers);
persistStore(store, {storage: AsyncStorage});
export default store;

import {loadCategories} from './modules/categories/actions';
const unsubscribe = store.subscribe(() =>
    console.log(store.getState())
);

store.dispatch(loadCategories());
